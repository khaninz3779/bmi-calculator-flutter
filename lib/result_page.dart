import 'package:bmi_calculator/custom_box.dart';
import 'package:flutter/material.dart';
import 'constants.dart';

class ResultPage extends StatelessWidget {

  final String bmiResult;
  final String bmi;
  final String interpretation;

  ResultPage({@required this.bmiResult,@required this.bmi,@required this.interpretation});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(12),
            child: Text('Your Result', style: kBoldLabel,
            ),
          ),
          Expanded(
            child: CustomBox(
              color: kInActiveCardColor,
              childWidget: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                children: [
                  Text(bmiResult,style: kResultTextStyle),
                  Text(bmi, style: kBMITextStyle),
                  Text(interpretation, style: kBodyTextStyle)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Container(
              height: kBtmBtnHeight.toDouble(),
              decoration: BoxDecoration(color: Colors.pink),
              child: Center(
                child: Text('Re-calculate', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
            ),
          )

        ],
      )
    );
  }
}
