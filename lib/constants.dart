import 'package:flutter/material.dart';

const kLabelTextStyle = TextStyle(
    fontSize: 18,
    color: Color(0xFF8D8E98)
);

const kBoldLabel = TextStyle(
    fontSize: 50,
    fontWeight: FontWeight.bold
);

const kResultTextStyle = TextStyle(
  fontSize: 22.0,
  color: Color(0xFF24D876),
  fontWeight: FontWeight.bold
);

const kBMITextStyle = TextStyle(
  fontSize: 100,
  fontWeight: FontWeight.bold
);

const kLargeButtonTextStyle = TextStyle(
  fontSize: 25,
  fontWeight: FontWeight.bold
);

const kBodyTextStyle = TextStyle(
  fontSize: 22.0
);


const Color kInActiveCardColor = Color(0xFF1D1E33);
const Color kActiveCardColor = Color(0xFF111328);
const int kBtmBtnHeight = 72;