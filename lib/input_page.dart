import 'package:bmi_calculator/calculator_brain.dart';
import 'package:bmi_calculator/result_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'icon_content.dart';
import 'custom_box.dart';
import 'constants.dart';

enum Gender { male, female }

Gender selectedGender = Gender.male;

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  void selectGender(Gender inputGender) {
    print(inputGender);
    setState(() {
      selectedGender = inputGender;
    });
  }

  Color isActiveColor(Gender buttonGender) {
    return selectedGender == buttonGender
        ? kActiveCardColor
        : kInActiveCardColor;
  }

  int height = 180;
  int weight = 60;
  int age = 25;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Expanded(
                    flex: 1,
                    child: CustomBox(
                      onTapFunction: () {
                        selectGender(Gender.male);
                      },
                      color: isActiveColor(Gender.male),
                      childWidget: IconContent(
                          icon: FontAwesomeIcons.mars, label: 'MALE'),
                    )),
                Expanded(
                    flex: 1,
                    child: CustomBox(
                        onTapFunction: () {
                          selectGender(Gender.female);
                        },
                        color: isActiveColor(Gender.female),
                        childWidget: IconContent(
                          icon: FontAwesomeIcons.venus,
                          label: 'FEMALE',
                        )))
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(children: [
              Expanded(
                flex: 1,
                child: CustomBox(color: kInActiveCardColor,
                  childWidget: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Height',style: kLabelTextStyle,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        textBaseline: TextBaseline.alphabetic,
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        children: [
                          Text(height.toString(),
                            style: kBoldLabel
                          ),
                          Text('cm', style: kLabelTextStyle,)
                      ],),
                      SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          activeTrackColor: Colors.white,
                          inactiveTrackColor: Color(0xFF8D8E98),
                          thumbColor: Color(0xFFEB1555),
                          overlayColor: Color(0x29EB1555),
                          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15),
                          overlayShape: RoundSliderOverlayShape(overlayRadius: 30)
                        ),
                        child: Slider(
                            value: height.toDouble(),
                            max: 200,
                            min: 120,
                            onChanged: (double newValue){
                              print(newValue);
                              setState(() {
                                height = newValue.round();
                              });
                            }
                        ),
                      ),
                    ],
                  )),
              ),
            ]),
          ),
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Expanded(flex: 1, child: CustomBox(
                  color: kInActiveCardColor,
                  childWidget: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Weight',style: kLabelTextStyle,),
                      Text(weight.toString(),style: kBoldLabel,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RoundIconButton(
                            icon:FontAwesomeIcons.minus,
                            onPressed: (){
                              setState(() {
                                weight--;
                              });
                          },),
                          SizedBox(width: 12),
                          RoundIconButton(
                            icon:FontAwesomeIcons.plus,
                            onPressed: (){
                              setState(() {
                                weight++;
                              });
                            },),
                        ],
                      )
                    ],
                  ),
                ),),
                Expanded(flex: 1, child: CustomBox(
                    color: kInActiveCardColor,
                  childWidget: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Age',style: kLabelTextStyle,),
                      Text(age.toString(),style: kBoldLabel,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RoundIconButton(
                            icon:FontAwesomeIcons.minus,
                            onPressed: (){
                              setState(() {
                                age--;
                              });
                            },),
                          SizedBox(width: 12),
                          RoundIconButton(
                            icon:FontAwesomeIcons.plus,
                            onPressed: (){
                              setState(() {
                                age++;
                              });
                            },),
                        ],
                      )
                    ],
                  ),
                ))
              ],
            ),
          ),
          GestureDetector(
            onTap: (){

              CalculatorBrain calc = CalculatorBrain(height:height, weight:weight);

              Navigator.push(context, MaterialPageRoute(builder: (context)=> ResultPage(
                  bmi: calc.calculateBMI(),
                  bmiResult: calc.bmiResult(),
                  interpretation: calc.getInterpretation()
              )));

            },
            child: Container(
              child: Center(
                child: Text(
                  'Calculate',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
              decoration: BoxDecoration(color: Colors.pink),
              width: double.infinity,
              height: kBtmBtnHeight.toDouble(),
              // padding: EdgeInsets.only(bottom: 20)
            ),
          )
        ],
      ),
    );
  }
}

class RoundIconButton  extends StatelessWidget {

  final Function onPressed;
  final IconData icon;

  RoundIconButton({@required this.onPressed, @required this.icon});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPressed,
      shape: CircleBorder(),
      constraints: BoxConstraints.tightFor(width: 56.0, height: 56.0),
      fillColor: Color(0xFF4C4F5E),
      child:Icon(icon)
    );
  }
}

