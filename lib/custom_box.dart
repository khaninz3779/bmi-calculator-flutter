import 'package:flutter/material.dart';

class CustomBox extends StatelessWidget {

  final Color color;
  final Widget childWidget;
  final Function onTapFunction;

  CustomBox({@required this.color, this.childWidget, this.onTapFunction});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapFunction,
      child: Container(
        child: childWidget,
        margin: EdgeInsets.all(15),
        width: double.infinity,
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(10)),
      ),
    );
  }
}